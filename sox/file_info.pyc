ó
öõZc           @   s  d  Z  d d l m Z d d l Z d d l m Z d d l m Z d d l m Z d d l m Z d	   Z	 d
   Z
 d   Z d   Z d   Z d   Z d   Z d   Z d d  Z d   Z d   Z d   Z d   Z d   Z d   Z d   Z d   Z d S(   s#    Audio file info computed by soxi.
i   (   t   loggeriÿÿÿÿN(   t   VALID_FORMATS(   t   soxi(   t   sox(   t   enquote_filepathc         C   sB   t  |   t |  d  } | d k r8 t j d |   n  t |  S(   sÿ   
    Number of bits per sample (0 if not applicable).

    Parameters
    ----------
    input_filepath : str
        Path to audio file.

    Returns
    -------
    bitrate : int
        number of bits per sample
        returns 0 if not applicable
    t   bt   0s   Bitrate unavailable for %s(   t   validate_input_fileR   R    t   warningt   int(   t   input_filepatht   output(    (    s   sox/file_info.pyt   bitrate   s
    
c         C   s#   t  |   t |  d  } t |  S(   s½   
    Show number of channels.

    Parameters
    ----------
    input_filepath : str
        Path to audio file.

    Returns
    -------
    channels : int
        number of channels
    t   c(   R   R   R	   (   R
   R   (    (    s   sox/file_info.pyt   channels#   s    
c         C   s#   t  |   t |  d  } t |  S(   s  
    Show file comments (annotations) if available.

    Parameters
    ----------
    input_filepath : str
        Path to audio file.

    Returns
    -------
    comments : str
        File comments from header.
        If no comments are present, returns an empty string.
    t   a(   R   R   t   str(   R
   R   (    (    s   sox/file_info.pyt   comments6   s    
c         C   sB   t  |   t |  d  } | d k r8 t j d |   n  t |  S(   s  
    Show duration in seconds (0 if unavailable).

    Parameters
    ----------
    input_filepath : str
        Path to audio file.

    Returns
    -------
    duration : float
        Duration of audio file in seconds.
        If unavailable or empty, returns 0.
    t   DR   s   Duration unavailable for %s(   R   R   R    R   t   float(   R
   R   (    (    s   sox/file_info.pyt   durationJ   s
    
c         C   s#   t  |   t |  d  } t |  S(   sÊ   
    Show the name of the audio encoding.

    Parameters
    ----------
    input_filepath : str
        Path to audio file.

    Returns
    -------
    encoding : str
        audio encoding type
    t   e(   R   R   R   (   R
   R   (    (    s   sox/file_info.pyt   encodinga   s    
c         C   s#   t  |   t |  d  } t |  S(   sÈ   
    Show detected file-type.

    Parameters
    ----------
    input_filepath : str
        Path to audio file.

    Returns
    -------
    file_type : str
        file format type (ex. 'wav')
    t   t(   R   R   R   (   R
   R   (    (    s   sox/file_info.pyt	   file_typet   s    
c         C   sB   t  |   t |  d  } | d k r8 t j d |   n  t |  S(   s  
    Show number of samples (0 if unavailable).

    Parameters
    ----------
    input_filepath : str
        Path to audio file.

    Returns
    -------
    n_samples : int
        total number of samples in audio file.
        Returns 0 if empty or unavailable
    t   sR   s$   Number of samples unavailable for %s(   R   R   R    R   R	   (   R
   R   (    (    s   sox/file_info.pyt   num_samples   s
    
c         C   s#   t  |   t |  d  } t |  S(   sÀ   
    Show sample-rate.

    Parameters
    ----------
    input_filepath : str
        Path to audio file.

    Returns
    -------
    samplerate : float
        number of samples/second
    t   r(   R   R   R   (   R
   R   (    (    s   sox/file_info.pyt   sample_rate   s    
gü©ñÒMbP?c         C   sQ   t  |   t |   } | d } | t d  k	 rI | | k rB t St Sn t Sd S(   s  
    Determine if an input file is silent.

    Parameters
    ----------
    input_filepath : str
        The input filepath.
    threshold : float
        Threshold for determining silence

    Returns
    -------
    is_silent : bool
        True if file is determined silent.
    s   Mean    normt   nanN(   R   t   statR   t   Falset   True(   R
   t	   thresholdt   stat_dictionaryt	   mean_norm(    (    s   sox/file_info.pyt   silent°   s    

c         C   sx   t  j j |   s* t d j |     n  t |   } | t k rt t j d d j	 t   t j
 d j |   n  d S(   s²   Input file validation function. Checks that file exists and can be
    processed by SoX.

    Parameters
    ----------
    input_filepath : str
        The input filepath.

    s!   input_filepath {} does not exist.s   Valid formats: %st    s-   This install of SoX cannot process .{} files.N(   t   ost   patht   existst   IOErrort   formatt   file_extensionR   R    t   infot   joinR   (   R
   t   ext(    (    s   sox/file_info.pyR   Ì   s    
c         C   s^   t  |  t  s t d   n! t |   d k  r? t d   n  x |  D] } t |  qF Wd S(   sá   Input file list validation function. Checks that object is a list and
    contains valid filepaths that can be processed by SoX.

    Parameters
    ----------
    input_filepath_list : list
        A list of filepaths.

    s#   input_filepath_list must be a list.i   s/   input_filepath_list must have at least 2 files.N(   t
   isinstancet   listt	   TypeErrort   lent
   ValueErrorR   (   t   input_filepath_listR
   (    (    s   sox/file_info.pyt   validate_input_file_listâ   s    
c         C   sí   t  t j j |    p1 t j t j   t j  t j t j j |   t j  g } t |  rz t d j	 |     n  t
 |   } | t k rÄ t j d d j t   t j d j	 |   n  t j j |   ré t j d |   n  d S(   sh  Output file validation function. Checks that file can be written, and
    has a valid file extension. Throws a warning if the path already exists,
    as it will be overwritten on build.

    Parameters
    ----------
    output_filepath : str
        The output filepath.

    Returns:
    --------
    output_filepath : str
        The output filepath.

    s&   SoX cannot write to output_filepath {}s   Valid formats: %sR%   s-   This install of SoX cannot process .{} files.s?   output_file: %s already exists and will be overwritten on buildN(   t   boolR&   R'   t   dirnamet   accesst   getcwdt   W_OKt   allR)   R*   R+   R   R    R,   R-   R   R(   (   t   output_filepatht   nowrite_conditionsR.   (    (    s   sox/file_info.pyt   validate_output_fileõ   s    %c         C   s   t  j j |   d d S(   s¶   Get the extension of a filepath.

    Parameters
    ----------
    filepath : str
        File path.

    Returns:
    --------
    extension : str
        The file's extension
    i   (   R&   R'   t   splitext(   t   filepath(    (    s   sox/file_info.pyR+     s    c         C   se   i t  |   d 6t |   d 6t |   d 6t |   d 6t |   d 6t |   d 6t |   d 6} | S(   s|  Get a dictionary of file information

    Parameters
    ----------
    filepath : str
        File path.

    Returns:
    --------
    info_dictionary : dict
        Dictionary of file information. Fields are:
            * channels
            * sample_rate
            * bitrate
            * duration
            * num_samples
            * encoding
            * silent
    R   R   R   R   R   R   R$   (   R   R   R   R   R   R   R$   (   R@   t   info_dictionary(    (    s   sox/file_info.pyR,   .  s    c         C   s   t  |   } t |  } | S(   sÏ   Returns a dictionary of audio statistics.

    Parameters
    ----------
    filepath : str
        File path.

    Returns
    -------
    stat_dictionary : dict
        Dictionary of audio statistics.
    (   t
   _stat_callt   _parse_stat(   R@   t   stat_outputR"   (    (    s   sox/file_info.pyR   N  s    c         C   s;   t  |   d t |   d d g } t |  \ } } } | S(   s²   Call sox's stat function.

    Parameters
    ----------
    filepath : str
        File path.

    Returns
    -------
    stat_output : str
        Sox output from stderr.
    R   s   -nR   (   R   R   R   (   R@   t   argst   _RD   (    (    s   sox/file_info.pyRB   `  s    
c         C   s   |  j  d  } i  } x | D]{ } | j  d  } t |  d k r | d } | d j d  } y t |  } Wn t k
 r d } n X| | | <q q W| S(   sæ   Parse the string output from sox's stat function

    Parameters
    ----------
    stat_output : str
        Sox output from stderr.

    Returns
    -------
    stat_dictionary : dict
        Dictionary of audio statistics.
    s   
t   :i   i    i   R%   N(   t   splitR2   t   stripR   R3   t   None(   RD   t   linest	   stat_dictt   linet
   split_linet   keyt   val(    (    s   sox/file_info.pyRC   s  s    

(   t   __doc__t   logR    R&   t   coreR   R   R   R   R   R   R   R   R   R   R   R   R$   R   R5   R>   R+   R,   R   RB   RC   (    (    (    s   sox/file_info.pyt   <module>   s.   											)		 		