#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

import socket
import sox
import subprocess
import time
import os

def bonjour():
    """
    Banner function, ascii art style 8)
    :return: No return
    """
    print('''
   ______   ______     ______     ______     ______     ______
  /\  == \ /\  __ \   /\  ___\   /\  ___\   /\  __ \   /\  ___\\
  \ \  _-/ \ \ \/\ \  \ \ \____  \ \___  \  \ \  __ \  \ \ \__ \\
   \ \_\    \ \_____\  \ \_____\  \/\_____\  \ \_\ \_\  \ \_____\\
    \/_/     \/_____/   \/_____/   \/_____/   \/_/\/_/   \/_____/
 _____     ______     ______     ______     _____     ______     ______
/\  __-.  /\  ___\   /\  ___\   /\  __ \   /\  __-.  /\  ___\   /\  == \\
\ \ \/\ \ \ \  __\   \ \ \____  \ \ \/\ \  \ \ \/\ \ \ \  __\   \ \  __<
 \ \____-  \ \_____\  \ \_____\  \ \_____\  \ \____-  \ \_____\  \ \_\ \_\\
  \/____/   \/_____/   \/_____/   \/_____/   \/____/   \/_____/   \/_/ /_/
    ''')

def netcat_udp(ip, port):
    '''
    Listening UDP server, similar to "nc -l -u <ip> <port>"
    :param ip: IP address to listen
    :param port: Port to listen
    :return: No return
    '''
    try:
        server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        server.bind(( ip, port ))
        print("[+] Listening on %s %s" % (ip, port))
    except:
        print("[-] Binding failed.")
        sys.exit(0)

    while True:
        data, addr = server.recvfrom(1024)
        #print(data)
        try:
            f = open('data.raw', 'ab+')
            f.write(data)
            f.close()
        except:
            print("[-] Writing 'data.raw' failed.")
            sys.exit(0)

    print("[+] Radio data stored in 'data.raw'")

def raw2decodable():
    '''
    First, this function will translate the raw GQRX stream into a valid POCSAG stream.
    Secondly, multimon-ng will decode the new stream and print the output.
    :param file: Output audio file from GQRX
    :return: No return
    '''
    # SoX conversion
    while True:
        sample = sox.Transformer()
        sample.set_input_format(file_type='raw', rate=48000, bits=16, encoding='signed-integer')
        sample.set_output_format(file_type='raw', rate=22050, bits=16, encoding='signed-integer')
        try:
            os.system('cls||clear')
            bonjour()
            print("[+] New entry:")
            sample.build('data.raw', 'raw2decode.raw')
            multimon('raw2decode.raw')
            time.sleep(10)
        except:
            pass

def multimon(file):
    '''
    Multimon-ng command.
    Similar to: "multimon-ng -t raw -a POCSAG512 -a POCSAG1200 -a POCSAG2400 -f alpha file"
    :param file: Raw file to decode (ouput of SoX)
    :return: No return
    '''
    test  = subprocess.Popen(['multimon-ng', '-t', 'raw', '-a', 'POCSAG512', '-a', 'POCSAG1200', '-a', 'POCSAG2400', '-f', 'alpha', file], stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
    res = test.communicate()[0].decode('utf-8').split('\n')
    f = open('output', 'w+')
    for line in res:
        if 'Alpha' in line:
            new_mess = line.split('Alpha:   ')[1]
            print("> %s" % new_mess)
            f.write("%s \n" % new_mess)
    f.close()
    os.remove(file)
