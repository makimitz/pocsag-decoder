# POCSAG Decoder

This script will decode GQRX POCSAG(512, 1200, 2400) stream. It's based on SoX (python API) and Multimon-ng (an ugly Popen). Then, not a very innovative script...

## Install

Easy to install :-)

### Packages

> On Debian based distro

```bash
# apt-get install gqrx sox multimon-ng
```

> On ArchLinux

```bash
$ yaourt -S gqrx sox multimon-ng
```

### Python libraries

```bash
pip3 install -r requirements.txt
```

## Usage

### GQRX

When your `HackRF` or `RTL-SDR` is detected:

```bash
$ hackrf_info
hackrf_info version: 2017.02.1
libhackrf version: 2017.02.1 (0.5)
Found HackRF
Index: 0
Serial number: XXXX
Board ID Number: 2 (HackRF One)
Firmware Version: 2015.07.2 (API:1.00)
Part ID Number: 0xa000cb3c 0x00724f5a
```

Launch GQRX and find a POCSAG signal, looks to:

<center>![pocag signal](https://www.sigidwiki.com/images/5/5b/POCSAG_Waterfallthmb.png)<br />
Fig 1 - POCSAG Signal</center>
Source : https://www.sigidwiki.com/wiki/Database

Range: 25 MHz — 932 MHz

### Signal decoding

When you're centered on your signal, toggle the `UDP` button on GQRX to send the radio stream on the port 7355 (default).

## Pocsag decoder

> Help menu

```bash
$ ./main.py --help
usage: main.py [-h] [-i IP] [-p PORT]

optional arguments:
  -h, --help            show this help message and exit
  -i IP, --ip IP        IP address to listen (GQRX default ip address:
                        127.0.0.1)
  -p PORT, --port PORT  Port to listen (GQRX default port: 7355)
```

> Decoding with default values

```bash
 ______   ______     ______     ______     ______     ______
/\  == \ /\  __ \   /\  ___\   /\  ___\   /\  __ \   /\  ___\
\ \  _-/ \ \ \/\ \  \ \ \____  \ \___  \  \ \  __ \  \ \ \__ \
 \ \_\    \ \_____\  \ \_____\  \/\_____\  \ \_\ \_\  \ \_____\
  \/_/     \/_____/   \/_____/   \/_____/   \/_/\/_/   \/_____/
 _____     ______     ______     ______     _____     ______     ______
/\  __-.  /\  ___\   /\  ___\   /\  __ \   /\  __-.  /\  ___\   /\  == \
\ \ \/\ \ \ \  __\   \ \ \____  \ \ \/\ \  \ \ \/\ \ \ \  __\   \ \  __<
 \ \____-  \ \_____\  \ \_____\  \ \_____\  \ \____-  \ \_____\  \ \_\ \_\
  \/____/   \/_____/   \/_____/   \/_____/   \/____/   \/_____/   \/_/ /_/

[+] New entry:
> Some
> POCSAG
> decoded
> data
```

Those results are stored in `output` file.

## TODO

* Don't store the raw stream of gqrx and decode from this file (`dump.raw`), use a global variable instead or something like that.
